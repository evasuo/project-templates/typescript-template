import {expect} from 'chai'
import main from './index'
describe("main()", () => {
    it("Outputs expected text", () => {
        expect(main()).to.eql("Complete");
    });
});
