# @evasuo/ts-template

[![coverage report](https://gitlab.com/evasuo/typescript-template/badges/master/coverage.svg?ignore_skipped=true)](https://gitlab.com/evasuo/uo-packets/-/commits/master)
[![pipeline status](https://gitlab.com/evasuo/typescript-template/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/evasuo/uo-packets/-/commits/master)


## Evasuo TS Template

A typescript template ready for testing, publishing to NPM, code coverage, and supporting both web and NodeJS buffers.

### Supports

* ✅ NodeJS
* ✅ Browser Javascript

### Contributing Guidelines

1. Use `yarn commit`
1. After committing, `yarn release-beta`, `yarn release-alpha`, or `yarn release`
1. Publishing (limited access) `yarn publish`

-- Setup

1. Add to GitLab Settings > CI/CD  > Test coverage parsing: `All files[^|]*\|[^|]*\s+([\d\.]+)`
1. `npm login` before first `yarn publish`
